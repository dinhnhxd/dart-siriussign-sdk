part of siriussign_sdk;

class TransactionListener {
  Listener listener;
  String apiNode;
  String ws;
  dynamic websocketInject;

  TransactionListener(String apiNode, [dynamic websocketInject]) {
    this.apiNode = apiNode;
    this.ws = NodeUtil.getWebsocketAddress(apiNode);
    this.websocketInject = websocketInject;
    this.listener = new Listener(this.ws);
  }

  /// Open websocket to listen events
  Future<Listener> open() {
    this.listener = new Listener(this.ws);
    return this.listener.open();
  }

  /// Check if the connection is still open
  bool check() {
    if (this.listener == null) return false;
    return (this.listener.websocket?.readyState == 1) ?? false;
  }

  /// Return current websocket state
  int getState() {
    return this.listener.websocket.readyState;
  }

  /// Check if the connection is still open, if not then reopen
  Future<void> checkAndOpenListener() async {
    if (!this.check()) {
      await this.open();
    }
  }

  /// Close websocket
  void close() {
    this.listener.close();
  }

  /// Announce a transaction to network
  /// @param signedTx
  Future<Object> announceTransaction(SignedTransaction signedTx) {
    SiriusClient client = SiriusClient.fromUrl(this.apiNode, null);
    return client.transaction.announce(signedTx);
  }

  /// Announce and wait for transaction confirmed or failed
  /// @param address
  /// @param signedTxHash
  /// @param fnAnnounce
  /// @param timeoutPolling
  Future<dynamic> waitForTransactionConfirmed(
      Address address, String signedTxHash, dynamic fnAnnounce,
      [int timeoutPolling = 45000]) async {
    Completer completer = new Completer();
    await this.checkAndOpenListener();
    Timer timeoutListener;
    this.listener.status(address).then((status) {
      timeoutListener.cancel();
      if (!completer.isCompleted) completer.completeError(status);
      this.close();
    }).catchError(
      (error) {},
    );

    this.listener.confirmed(address).then((successTx) {
      if ((successTx != null) &&
          (successTx.toJson()['abstractTransaction']['transactionHash'] ==
              signedTxHash)) {
        timeoutListener.cancel();
        if (!completer.isCompleted) completer.complete(successTx);
        this.close();
      }
    }).catchError((error) {});

    fnAnnounce();

    timeoutListener = Timer(Duration(milliseconds: timeoutPolling), () async {
      if (completer.isCompleted) return;
      this.close();
      SiriusClient client = SiriusClient.fromUrl(this.apiNode, null);
      TransactionStatus status =
          await client.transaction.getTransactionStatus(signedTxHash);

      if (status.group == 'unconfirmed') {
        Timer.periodic(Duration(milliseconds: 500), (timer) async {
          TransactionStatus statusReget =
              await client.transaction.getTransactionStatus(signedTxHash);
          if (statusReget.group == 'confirmed') {
            Transaction tx =
                await client.transaction.getTransaction(signedTxHash);
            if (!completer.isCompleted) completer.complete(tx);
            timer.cancel();
          }
          if (statusReget.group == 'failed') {
            if (!completer.isCompleted) completer.completeError(statusReget);
            timer.cancel();
          }
        });
      }

      if (status.group == 'confirmed') {
        Transaction tx = await client.transaction.getTransaction(signedTxHash);
        if (!completer.isCompleted) completer.complete(tx);
      }

      if (status.group == 'failed') {
        if (!completer.isCompleted) completer.completeError(status);
      }
    });
    return completer.future.then((value) => value);
  }
}
