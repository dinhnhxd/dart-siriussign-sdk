part of siriussign_sdk;

abstract class SiriusSignMessage {
  /// Message Header to identify the transaction belongs to the app
  SiriusSignMessageHeader header;

  Map<String, dynamic> body;

  Map<String, dynamic> meta;

  SiriusSignMessage(String appID, int messageType) {
    this.header = SiriusSignMessageHeader(
      appID,
      APPLICATION,
      SDK_VERSION,
      messageType,
    );
  }

  /// Create transaction Plain Message from Sirius Sign Message
  PlainMessage toPlainMessage() {
    final Map<String, dynamic> message = {
      "header": this.header.toJson(),
      "body": this.body,
      "meta": this.meta
    };
    var messageString = jsonEncode(message);
    return PlainMessage(payload: messageString);
  }

  /// Create transaction message string from Sirius Sign Message
  String toMessage() {
    var message = {"header": this.header, "body": this.body, "meta": this.meta};
    return jsonEncode(message);
  }

  static bool isSiriusSignMessage(String payload) {
    SiriusSignMessage message;
    try {
      message = jsonDecode(payload);
    } catch (e) {
      return false;
    }
    if (message.header?.application != APPLICATION) return false;
    return true;
  }

  static int getType(String payload) {
    Map<String, dynamic> message;
    try {
      message = jsonDecode(payload);
    } catch (e) {
      throw ('M01: Payload is not a Sirius Sign message string');
    }
    if (message['header']['application'] != APPLICATION ?? true)
      throw ('M01: Payload is not a Sirius Sign message string');
    if ((message['header']['messageType'] == null))
      throw ('M01: Payload is not a Sirius Sign message string');
    return message['header']['messageType'];
  }
}
