import 'package:flutter_test/flutter_test.dart';
import 'package:siriussign_sdk/siriussign_sdk.dart';

void main() {
  group('Create Sirius Sign Document', () {
    test('should create a right Document', () {
      final dataUri = 'data:text/plain;base64,VGhpcyBpcyBhIHRleHQgZmlsZS4K';
      final data = ConverterUtil.dataURIToUint8Array(dataUri);
      final ssDoc =
          SiriusSignDocument.createFromFile('test.txt', 'text/plain', data);

      // expect(ssDoc).to.be.a.instanceof(SiriusSignDocument);
      expect(ssDoc.fileDataUri, dataUri);
      expect(
          ssDoc.fileHash,
          'fd8b11f530783ea9623c505ad9a870ce574ddb51c1f58252df3482aaf7025a9b'
              .toUpperCase());
    });

    test('getCosignerAddresses should return empty with blank document', () {
      final dataUri = 'data:text/plain;base64,VGhpcyBpcyBhIHRleHQgZmlsZS4K';
      final data = ConverterUtil.dataURIToUint8Array(dataUri);
      final ssDoc =
          SiriusSignDocument.createFromFile('test.txt', 'text/plain', data);
      final cosignerAddresses = ssDoc.info.getCosignerAddresses();
      expect(cosignerAddresses, []);
    });

    test('getSignedCosignerAddresses should return empty with blank document',
        () {
      final dataUri = 'data:text/plain;base64,VGhpcyBpcyBhIHRleHQgZmlsZS4K';
      final data = ConverterUtil.dataURIToUint8Array(dataUri);
      final ssDoc =
          SiriusSignDocument.createFromFile('test.txt', 'text/plain', data);
      final cosignatures = ssDoc.info.getSignedCosignerAddresses();
      expect(cosignatures, []);
    });

    test('checkComplete should return false with blank document', () {
      final dataUri = 'data:text/plain;base64,VGhpcyBpcyBhIHRleHQgZmlsZS4K';
      final data = ConverterUtil.dataURIToUint8Array(dataUri);
      final ssDoc =
          SiriusSignDocument.createFromFile('test.txt', 'text/plain', data);
      final isComplete = ssDoc.info.checkIsCompleted();
      expect(isComplete, false);
    });
  });
}
