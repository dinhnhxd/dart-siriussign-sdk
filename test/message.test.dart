import 'package:flutter_test/flutter_test.dart';
import 'package:siriussign_sdk/siriussign_sdk.dart';
import 'package:xpx_chain_sdk/xpx_sdk.dart';
import 'constant.test.dart';

void main() {
  group('Create Sirius Sign Document Signing Message', () {
    test('should create a Document Signing Message object', () {
      final docSigningMsg = new SiriusSignDocumentSigningMessage(
          APP_ID,
          'TESTFILEHASH',
          'Test File Name.pdf',
          'application/pdf',
          true,
          'TESTUPLOADSTRINGHASH',
          EncryptType.PLAIN,
          'SIGNATUREUPLOADTXHASH',
          {'signatureUploadTxHash': 'TESTSIGNATUREUPLOADTXHASH'});

      expect(docSigningMsg is SiriusSignDocumentSigningMessage, true);
    });

    test('should create a Document Signing Message object evenif no meta input',
        () {
      final docSigningMsg = new SiriusSignDocumentSigningMessage(
        APP_ID,
        'TESTFILEHASH',
        'Test File Name.pdf',
        'application/pdf',
        true,
        'TESTUPLOADSTRINGHASH',
        EncryptType.PLAIN,
        'SIGNATUREUPLOADTXHASH',
      );

      expect(docSigningMsg is SiriusSignDocumentSigningMessage, true);
    });

    test('should throw an error if input payload is not a Sirius Sign message',
        () {
      try {
        SiriusSignDocumentSigningMessage.createFromPayload(
            'Any message payload');
        assert('expected exception not thrown' != '');
      } catch (e) {
        expect(e, 'M01: Payload is not a Sirius Sign message string');
      }

      try {
        SiriusSignDocumentSigningMessage.createFromPayload('');
        assert('' != 'expected exception not thrown');
      } catch (e) {
        expect(e, 'M01: Payload is not a Sirius Sign message string');
      }

      try {
        SiriusSignDocumentSigningMessage.createFromPayload(
            '{"header":{"application":"Another application"}}');
        assert('' != 'expected exception not thrown');
      } catch (e) {
        expect(e, 'M01: Payload is not a Sirius Sign message string');
      }
    });

    test(
        'should throw an error if input payload is not a Document Signing Message',
        () {
      try {
        SiriusSignDocumentSigningMessage.createFromPayload(
            '{"header": {"appID":"AppName", "application":"SiriusSign", "version":"0.0.1", "messageType": "0"}, "body": {}, "meta": {}}');
        assert('' != 'expected exception not thrown');
      } catch (e) {
        expect(e,
            'M02: This Sirius Sign Message is not a Document Signing Message');
      }
    });

    // test('should throw an error if input payload is a Document Signing Message but missing properties', () {
    //     try {
    //         final a = SiriusSignDocumentSigningMessage.createFromPayload('{"header": {"appID":"AppName", "application":"SiriusSign", "version":"0.0.1", "messageType": "1"}, "body": {}, "meta": {}}')
    //         assert('' != 'expected exception not thrown');
    //     }
    //     catch (e) {
    //         expect(e, 'M02: This Sirius Sign Message is not a Document Signing Message');
    //     }
    // });
  });

  group('Create Sirius Sign Cosigner Notifying Message', () {
    test('should create a Document Signing Message object', () {
      final docSigningMsg = new SiriusSignCosignerNotifyingMessage(
          APP_ID,
          'TESTCOSIGNERNOTIFYINGHASH',
          {'signatureUploadTxHash': 'TESTSIGNATUREUPLOADTXHASH'});

      expect(docSigningMsg is SiriusSignCosignerNotifyingMessage, true);
    });

    test('should throw an error if input payload is not a Sirius Sign message',
        () {
      try {
        SiriusSignCosignerNotifyingMessage.createFromPayload(
            'Any message payload');
        assert('' != 'expected exception not thrown');
      } catch (e) {
        expect(e, 'M01: Payload is not a Sirius Sign message string');
      }

      try {
        SiriusSignCosignerNotifyingMessage.createFromPayload('');
        assert('' != 'expected exception not thrown');
      } catch (e) {
        expect(e, 'M01: Payload is not a Sirius Sign message string');
      }

      try {
        SiriusSignCosignerNotifyingMessage.createFromPayload(
            '{"header":{"application":"Another application"}}');
        assert('' != 'expected exception not thrown');
      } catch (e) {
        expect(e, 'M01: Payload is not a Sirius Sign message string');
      }
    });

    test(
        'should throw an error if input payload is not a Document Signing Message',
        () {
      try {
        SiriusSignCosignerNotifyingMessage.createFromPayload(
            '{"header": {"appID":"AppName", "application":"SiriusSign", "version":"0.0.1", "messageType": "1"}, "body": {}, "meta": {}}');
        assert('' != 'expected exception not thrown');
      } catch (e) {
        expect(e,
            'M03: This Sirius Sign Message is not a Cosigner Notifying Message');
      }
    });
  });

  group('Create Sirius Sign Document Verifying Message', () {
    test('should create a Document Signing Message object', () {
      final docSigningMsg = new SiriusSignDocumentVerifyingMessage(
          APP_ID,
          'TESTFILEHASH',
          'Test File Name.pdf',
          'application/pdf',
          true,
          'TESTUPLOADSTRINGHASH',
          EncryptType.PLAIN,
          {'signatureUploadTxHash': 'TESTSIGNATUREUPLOADTXHASH'});

      expect(docSigningMsg is SiriusSignDocumentVerifyingMessage, true);
    });

    test('should create a Document Signing Message object evenif no meta input',
        () {
      final docSigningMsg = new SiriusSignDocumentVerifyingMessage(
          APP_ID,
          'TESTFILEHASH',
          'Test File Name.pdf',
          'application/pdf',
          true,
          'TESTUPLOADSTRINGHASH',
          EncryptType.PLAIN);

      expect(docSigningMsg is SiriusSignDocumentVerifyingMessage, true);
    });

    test('should throw an error if input payload is not a Sirius Sign message',
        () {
      try {
        SiriusSignDocumentVerifyingMessage.createFromPayload(
            'Any message payload');
        assert('' != 'expected exception not thrown');
      } catch (e) {
        expect(e, 'M01: Payload is not a Sirius Sign message string');
      }

      try {
        SiriusSignDocumentVerifyingMessage.createFromPayload('');
        assert('' != 'expected exception not thrown');
      } catch (e) {
        expect(e, 'M01: Payload is not a Sirius Sign message string');
      }

      try {
        SiriusSignDocumentVerifyingMessage.createFromPayload(
            '{"header":{"application":"Another application"}}');
        assert('' != 'expected exception not thrown');
      } catch (e) {
        expect(e, 'M01: Payload is not a Sirius Sign message string');
      }
    });

    test(
        'should throw an error if input payload is not a Document Signing Message',
        () {
      try {
        SiriusSignDocumentVerifyingMessage.createFromPayload(
            '{"header": {"appID":"AppName", "application":"SiriusSign", "version":"0.0.1", "messageType": "0"}, "body": {}, "meta": {}}');
        assert('' != 'expected exception not thrown');
      } catch (e) {
        expect(e,
            'M04: This Sirius Sign Message is not a Document Verifying Message');
      }
    });
  });

  group('Create Sirius Sign Verifier Notifying Message', () {
    test('should create a Document Signing Message object', () {
      final docSigningMsg = new SiriusSignVerifierNotifyingMessage(
          APP_ID,
          'TESTCOSIGNERNOTIFYINGHASH',
          {'signatureUploadTxHash': 'TESTSIGNATUREUPLOADTXHASH'});

      expect(docSigningMsg is SiriusSignVerifierNotifyingMessage, true);
    });

    test('should throw an error if input payload is not a Sirius Sign message',
        () {
      try {
        SiriusSignVerifierNotifyingMessage.createFromPayload(
            'Any message payload');
        assert('' != 'expected exception not thrown');
      } catch (e) {
        expect(e, 'M01: Payload is not a Sirius Sign message string');
      }

      try {
        SiriusSignVerifierNotifyingMessage.createFromPayload('');
        assert('' != 'expected exception not thrown');
      } catch (e) {
        expect(e, 'M01: Payload is not a Sirius Sign message string');
      }

      try {
        SiriusSignVerifierNotifyingMessage.createFromPayload(
            '{"header":{"application":"Another application"}}');
        assert('' != 'expected exception not thrown');
      } catch (e) {
        expect(e, 'M01: Payload is not a Sirius Sign message string');
      }
    });

    test(
        'should throw an error if input payload is not a Document Signing Message',
        () {
      try {
        SiriusSignVerifierNotifyingMessage.createFromPayload(
            '{"header": {"appID":"AppName", "application":"SiriusSign", "version":"0.0.1", "messageType": "1"}, "body": {}, "meta": {}}');
        assert('' != 'expected exception not thrown');
      } catch (e) {
        expect(e,
            'M05: This Sirius Sign Message is not a Verifier Notifying Message');
      }
    });
  });

  group('Create Plain Message from Sirius Sign Message', () {
    test('should create a PlainMessage contain Document Signing Message', () {
      final docSigningMsg = new SiriusSignDocumentSigningMessage(
          APP_ID,
          'TESTFILEHASH',
          'Test File Name.pdf',
          'application/pdf',
          true,
          'TESTUPLOADSTRINGHASH',
          EncryptType.PLAIN,
          'SIGNATUREUPLOADTXHASH',
          {'isMulti': true});
      final plainMsg = docSigningMsg.toPlainMessage();

      expect(plainMsg is PlainMessage, true);
    });

    test('should create a PlainMessage contain Cosigner Notifying Message', () {
      final docSigningMsg = new SiriusSignCosignerNotifyingMessage(
          APP_ID, 'TESTFILEHASH', {'fileHash': 'TESTFILEHASH'});
      final plainMsg = docSigningMsg.toPlainMessage();

      expect(plainMsg is PlainMessage, true);
    });

    test('should create a PlainMessage contain Document Verifying Message', () {
      final docSigningMsg = new SiriusSignDocumentVerifyingMessage(
          APP_ID,
          'TESTFILEHASH',
          'Test File Name.pdf',
          'application/pdf',
          true,
          'TESTUPLOADSTRINGHASH',
          EncryptType.PLAIN,
          {'isMulti': true});
      final plainMsg = docSigningMsg.toPlainMessage();

      expect(plainMsg is PlainMessage, true);
    });

    test('should create a PlainMessage contain Verifier Notifying Message', () {
      final docSigningMsg =
          new SiriusSignVerifierNotifyingMessage(APP_ID, 'TESTFILEHASH', {
        'fileHash': 'TESTFILEHASH',
      });
      final plainMsg = docSigningMsg.toPlainMessage();

      expect(plainMsg is PlainMessage, true);
    });
  });
}
