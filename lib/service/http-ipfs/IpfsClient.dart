part of siriussign_sdk.ipfs;

class IpfsClient {
  final http.Client _client;
  final String _baseApiUri;

  IpfsClient(this._baseApiUri, this._client);

  IpfsClient.fromUri(this._baseApiUri) : _client = http.IOClient();

  Future<StreamView<List<int>>> cat(String arg) async {
    var response = await _get('/api/v0/cat?arg=$arg');
    return response.stream;
  }

  Future<Uint8List> getData(String arg) async {
    var response = await _get('/api/v0/cat?arg=$arg');
    return await response.stream.toBytes();
  }

  Future<http.ByteStream> getDataStream(String arg) async {
    var response = await _get('/api/v0/cat?arg=$arg');
    return response.stream;
  }

  Future<String> catString(String arg) async {
    var response = await _get('/api/v0/cat?arg=$arg');
    return await utf8.decodeStream(response.stream);
  }

  Future<AddResponse> addString(String content) async {
    var part = http.MultipartFile.fromString('file', content);
    var res = await _multipart('/api/v0/add', [part]);
    var body = await utf8.decodeStream(res.stream);
    return AddResponse.fromJson(jsonDecode(body));
  }

  Future<AddResponse> addBytes(Uint8List content) async {
    var part = http.MultipartFile.fromBytes('file', content);
    var res = await _multipart('/api/v0/add', [part]);
    var body = await utf8.decodeStream(res.stream);
    return AddResponse.fromJson(jsonDecode(body));
  }

  Future<AddResponse> addStream(Stream<List<int>> content, int length) async {
    var part = http.MultipartFile('file', content, length);
    var res = await _multipart('/api/v0/add', [part]);
    var body = await utf8.decodeStream(res.stream);
    return AddResponse.fromJson(jsonDecode(body));
  }

  Future<http.StreamedResponse> _get(String path) async {
    var uri = Uri.parse('$_baseApiUri$path');
    var req = http.Request('GET', uri);
    return _client.send(req);
  }

  Future<http.StreamedResponse> _multipart(
      String path, Iterable<http.MultipartFile> parts) async {
    var uri = Uri.parse('$_baseApiUri$path');
    var req = http.MultipartRequest('POST', uri)..files.addAll(parts);
    return _client.send(req);
  }
}
