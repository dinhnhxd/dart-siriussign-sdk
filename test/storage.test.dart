import 'package:flutter_test/flutter_test.dart';
import 'package:siriussign_sdk/service/http-ipfs/HttpIpfs.dart';
import 'package:siriussign_sdk/siriussign_sdk.dart';
import 'package:xpx_chain_sdk/xpx_sdk.dart';
import './constant.test.dart';

void main() {
  group('Upload/Download a Sirius Sign document', () {
    test('should upload a plain file then download the same file', () async {
      final docStorage = new DocumentStorage(API_NODE, IPFS_NODE, NETWORK_TYPE);
      final dataUri = 'data:text/plain;base64,VGhpcyBpcyBhIHRleHQgZmlsZS4K';
      final data = ConverterUtil.dataURIToUint8Array(dataUri);
      final ssDoc =
          SiriusSignDocument.createFromFile('test.txt', 'text/plain', data);
      final ownerAcc =
          Account.fromPrivateKey(PRIVATE_KEY['privateTest_1'], NETWORK_TYPE);
      final docAcc =
          Account.fromPrivateKey(PRIVATE_KEY['privateTest_2'], NETWORK_TYPE);
      ssDoc.setDocumentAccount(docAcc).setEncryptType(EncryptType.PLAIN);
      final sleep = (int ms) => new Future.delayed(Duration(milliseconds: ms));

      // Upload
      final uploadResult = await docStorage.uploadDocument(ssDoc, ownerAcc);
      await sleep(10000); // Waiting for transaction confirmed
      // Download
      final DownloadResult downloadResult = await docStorage.downloadDocument(
          uploadResult.hash, EncryptType.PLAIN);

      final downloadData = await downloadResult.data?.getContentAsBytes();
      final base64Data = ConverterUtil.uint8ArrayToBase64(downloadData);
      final fileType = downloadResult.data?.contentType;
      final downloadedDataUri = 'data:' + fileType + ';base64,' + base64Data;

      expect(downloadedDataUri, dataUri);
    }, timeout: Timeout.parse('200000ms'));

    test('should upload a file then download the same file with NEMKeyPrivacy',
        () async {
      final docStorage = new DocumentStorage(API_NODE, IPFS_NODE, NETWORK_TYPE);
      final dataUri = 'data:text/plain;base64,VGhpcyBpcyBhIHRleHQgZmlsZS4K';
      final data = ConverterUtil.dataURIToUint8Array(dataUri);
      final ssDoc =
          SiriusSignDocument.createFromFile('test.txt', 'text/plain', data);
      final ownerAcc =
          Account.fromPrivateKey(PRIVATE_KEY['privateTest_1'], NETWORK_TYPE);
      final docAcc =
          Account.fromPrivateKey(PRIVATE_KEY['privateTest_2'], NETWORK_TYPE);
      ssDoc.setDocumentAccount(docAcc).setEncryptType(EncryptType.KEYPAIR);
      final sleep = (int ms) => new Future.delayed(Duration(milliseconds: ms));

      // Uplaod
      final uploadResult = await docStorage.uploadDocument(ssDoc, ownerAcc);
      await sleep(10000); // Waiting for transaction confirmed
      // Download
      final DownloadResult downloadResult = await docStorage.downloadDocument(
          uploadResult.hash, EncryptType.KEYPAIR,
          uploader: ownerAcc);

      final downloadData = await downloadResult.data?.getContentAsBytes();
      final base64Data = ConverterUtil.uint8ArrayToBase64(downloadData);
      final fileType = downloadResult.data?.contentType;
      final downloadedDataUri = 'data:' + fileType + ';base64,' + base64Data;

      expect(downloadedDataUri, dataUri);
    }, timeout: Timeout.parse('200000ms'));
  });
}
