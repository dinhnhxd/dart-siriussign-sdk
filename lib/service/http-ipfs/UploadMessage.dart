part of siriussign_sdk.ipfs;

class UploadMessage {
  int privacyType;
  String contentType;
  String dataHash;
  String description;
  Map<String, String> metadata;
  String name;
  int timestamp;
  String version = "1.0";

  UploadMessage(
    this.privacyType,
    this.contentType,
    this.dataHash,
    this.description,
    this.metadata,
    this.name,
  );

  PlainMessage getPlainMessage() {
    final payload = json.encode({
      "privacyType": this.privacyType,
      "data": {
        "contentType": this.contentType,
        "dataHash": this.dataHash,
        "description": this.description,
        "metadata": this.metadata,
        "name": this.name,
        "timestamp": DateTime.now().millisecondsSinceEpoch
      },
      "version": "1.0"
    });

    final message = PlainMessage(payload: payload);
    return message;
  }

  UploadMessage.fromJson(Map<String, dynamic> payload) {
    this.privacyType = payload["privacyType"];
    this.contentType = payload["data"]["contentType"];
    this.dataHash = payload["data"]["dataHash"];
    this.description = payload["data"]["description"];
    this.metadata = Map<String, String>.from(payload["data"]["metadata"]);
    this.name = payload["data"]["name"];
    this.timestamp = payload["data"]["timestamp"];
  }
}
