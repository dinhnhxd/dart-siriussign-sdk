part of siriussign_sdk;

class ConverterUtil {
  /// Convert base64 string to hex string
  /// @param base64
  // static String base64ToHex(String base64) {
  //     // let raw = Buffer.from(base64, 'base64').toString(); // atob(base64);
  //     // let hex = '';
  //     // for (let i = 0; i < raw.length; i++) {
  //     //     let _hex = raw.codeUnitAt(i).toString(16)
  //     //     hex += (_hex.length == 2 ? _hex : '0' + _hex);
  //     // }
  //     // return hex;
  //     return Buffer.from(base64, 'base64').toString('hex');
  // }

  /// Convert hex string to base64 string
  /// @param hex
  // static hexToBase64(String hex) {
  //     // return btoa(String.fromCharCode.apply(null,
  //     //     hex.replace(/\r|\n/g, "").replace(/([\da-fA-F]{2}) ?/g, "0x$1 ").replace(/ +$/, "").split(" "))
  //     // );
  //     return Buffer.from(hex, 'hex').toString('base64');
  // }

  static String uint8ListToHex(Uint8List bytes) {
    return bytes
        .map((e) {
          final hexRaw = e.toRadixString(16);
          final hex8 = (hexRaw.length == 2) ? hexRaw : ('0' + hexRaw);
          return hex8;
        })
        .join('')
        .toUpperCase();
  }

  static Uint8List hexToUint8Array(String hex) {
    if (0 != hex.length % 2) {
      throw ('hex string has unexpected size ${hex.length}');
    }
    List<int> result = [];
    for (var i = 0; i < hex.length - 1; i += 2)
      result.add(int.parse(hex[i] + hex[i + 1], radix: 16));
    return Uint8List.fromList(result);
  }

  /// Convert bytes array to base64
  static String uint8ArrayToBase64(Uint8List bytes) {
    return base64.encode(bytes);
  }

  /// Convert data URI to Base64 Unit8Array
  /// @param dataURI
  static Uint8List dataURIToUint8Array(String dataURI) {
    const BASE64_MARKER = ';base64,';
    final base64Index = dataURI.indexOf(BASE64_MARKER) + BASE64_MARKER.length;
    final base64Str = dataURI.substring(base64Index);
    // final raw = Buffer.from(base64, 'base64').toString(); // window.atob(base64);
    // final rawLength = raw.length;
    // final array = new Uint8List(new ArrayBuffer(rawLength));

    // for (var i = 0; i < rawLength; i++) {
    //     array[i] = raw.codeUnitAt(i);
    // }
    final array = base64.decode(base64Str);
    return array;
  }

  /// Convert base64 data string to base64 dataURI
  /// @param base64
  /// @param fileType
  static base64ToDataURI(String base64, String fileType) {
    return 'data:' + fileType + ';base64,' + base64;
  }
}
