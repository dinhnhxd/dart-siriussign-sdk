part of siriussign_sdk;

class SimpleParsedTransaction {
  Uint64 height;
  int index;
  String id;
  String transactionHash;
  String merkleComponentHash;
  String aggregateHash;
  String aggregateId;

  int networkType;
  int type;
  int version;
  Uint64 maxFee;
  Deadline deadline;
  String signature;
  PublicAccount signer;
  Address recipient;
  List<Mosaic> mosaics;
  Message message;

  dynamic modifications;

  SimpleParsedTransaction(
    this.networkType,
    this.type,
    this.version,
    this.maxFee,
    this.deadline,
    this.signature,
    this.signer,
    this.recipient,
    this.mosaics,
    this.message,
  );

  SimpleParsedTransaction.fromTransaction(Transaction tx) {
    final txJson = tx.toJson();
    final abTxJson = txJson["abstractTransaction"];

    // Transaction Info
    this.height = abTxJson["height"];
    this.index = abTxJson["index"];
    this.id = abTxJson["id"];
    this.transactionHash = abTxJson["transactionHash"];
    this.merkleComponentHash = abTxJson["merkleComponentHash"];
    this.aggregateHash = abTxJson["aggregateHash"];
    this.aggregateId = abTxJson["aggregateId"];
    // Other Transaciton properties
    this.networkType = abTxJson["networkType"];
    this.type = (abTxJson["type"] as TransactionType).value;
    this.version = abTxJson["version"];
    this.maxFee = abTxJson["maxFee"];
    this.deadline = abTxJson["deadline"];
    this.signature = abTxJson["signature"];
    this.signer = abTxJson["signer"];
    this.recipient = (txJson["recipient"] != null)
        ? Address.fromRawAddress(txJson["recipient"]["address"])
        : null;
    this.mosaics = (txJson["mosaics"] != null)
        ? (txJson["mosaics"] as List).cast<Mosaic>()
        : null;
    this.message = txJson["message"];
  }

  SimpleParsedTransaction.fromListener(Map<String, dynamic> tx) {
    // final tx = json.decode(txString);
    this.recipient = (tx['transaction']['recipient'] != null)
        ? Address.fromEncoded(tx['transaction']['recipient'])
        : null;
    this.modifications = (tx['transaction']['modifications'] != null)
        ? tx['transaction']['modifications']
        : null;
    this.networkType = (this.recipient != null)
        ? this.recipient.networkType
        : (this.modifications != null)
            ? Address.fromEncoded(this.modifications[0]['value']).networkType
            : null;
    this.type = tx['transaction']['type'];
    this.version = tx['transaction']['version'];
    this.maxFee = Uint64.fromInts(
        tx['transaction']['maxFee'][0], tx['transaction']['maxFee'][1]);
    this.deadline = Deadline.fromUInt64DTO(
        UInt64DTO.fromJson(tx['transaction']['deadline']));
    this.signature = tx['transaction']['signature'];
    this.signer = PublicAccount.fromPublicKey(
        tx['transaction']['signer'], this.networkType);
    this.mosaics = tx['transaction']['mosaics'] ?? [];
    this.message = (tx['transaction']['message'] != null)
        ? Message.fromDTO(MessageDTO.fromJson(tx['transaction']['message']))
        : null;

    final metaJson = tx['meta'];
    this.height = Uint64.fromDto(UInt64DTO.fromJson(metaJson["height"]));
    this.index = metaJson["index"];
    this.id = metaJson["id"];
    this.transactionHash = metaJson["hash"];
    this.merkleComponentHash = metaJson["merkleComponentHash"];
    this.aggregateHash = metaJson["aggregateHash"] ?? null;
    this.aggregateId = metaJson["aggregateId"] ?? null;
  }
}
