part of siriussign_sdk.ipfs;

class SHA3Hasher {
  /// Calculates the hash of data.
  /// @param {Uint8Array} dest The computed hash destination.
  /// @param {Uint8Array} data The data to hash.
  /// @param {numeric} length The hash length in bytes.
  /// @param {SignSchema} signSchema The Sign Schema. (KECCAK_REVERSED_KEY / SHA3)
  static Uint8List func(Uint8List dest, Uint8List data, int length,
      [int signSchema = SignSchema.SHA3]) {
    final hasher = SHA3Hasher.getHasher(length, signSchema);
    final hash = hasher.process(data);
    dest = Uint8List.fromList(hash);
    return dest;
  }

  /// Creates a hasher object.
  /// @param {numeric} length The hash length in bytes.
  /// @param {SignSchema} signSchema The Sign Schema. (KECCAK_REVERSED_KEY / SHA3)
  /// @returns {object} The hasher.
  static createHasher([length = 64, signSchema = SignSchema.SHA3]) {
    var hash;
    return {
      "reset": () => {
            hash = SHA3Hasher.getHasher(length, signSchema) // .create();
          },
      "update": (data) => {
            if (data is Uint8List)
              {hash.update(data)}
            else if (data is String)
              {hash.update(int.parse(data, radix: 16))}
            else
              {throw ('unsupported data type')}
          },
      "finalize": (result) => {result = Uint8List.fromList(hash)},
    };
  }

  /// Get a hasher instance.
  /// @param {numeric} length The hash length in bytes.
  /// @param {SignSchema} signSchema The Sign Schema. (KECCAK_REVERSED_KEY / SHA3)
  /// @returns {object} The hasher.
  static getHasher([length = 64, signSchema = SignSchema.SHA3]) {
    if (signSchema == SignSchema.KECCAK_REVERSED_KEY)
      throw ('SignSchema.KECCAK_REVERSED_KEY is not supported');
    if (length == 32) return sha3.New256();
    if (length == 64) return sha3.New512();
  }
}
