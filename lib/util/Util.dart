part of siriussign_sdk;

typedef AsyncForEachCallback = Future<Null> Function(dynamic element, int index,
    [List<dynamic> listOfElements]);

typedef AsyncForEachBreakFn = bool Function();

class Util {
  /// Delay in ms (ms)
  /// @param ms
  static sleep(int ms) {
    return Future.delayed(Duration(milliseconds: ms));
  }

  /// Asynchronous forEach
  /// @param array
  /// @param callback
  static asyncForEach(List<dynamic> array, AsyncForEachCallback callback,
      [AsyncForEachBreakFn breakFn]) async {
    var breakCallback;
    if (breakFn != null)
      breakCallback = breakFn;
    else
      breakCallback = () => false;
    for (var index = 0; index < array.length; index++) {
      if (breakCallback()) break;
      await callback(array[index], index, array);
    }
  }
}
