part of siriussign_sdk;

class SiriusSignDocument {
  Account documentAccount;
  String signatureUploadTxHash;
  List<int> completedDocumentData;
  String fileName;
  String fileType;
  List<int> fileData;
  String fileDataUri;
  String fileHash;
  SiriusSignDocumentInfo info;

  SiriusSignDocument._(this.fileName, this.fileType, this.fileData,
      this.fileDataUri, this.fileHash, this.info);

  static SiriusSignDocument createFromFile(
      String fileName, String fileType, List<int> fileData) {
    String fileDataUri =
        'data:' + fileType + ';base64,' + base64.encode(fileData);

    List<int> fileDataUriInList = utf8.encode(fileDataUri);
    String fileHash =
        sha256.convert(fileDataUriInList).toString().toUpperCase();
    SiriusSignDocumentInfo initInfo =
        SiriusSignDocumentInfo.init(fileName, fileType, fileHash);
    return new SiriusSignDocument._(
        fileName, fileType, fileData, fileDataUri, fileHash, initInfo);
  }

  static SiriusSignDocument createFromInfo(SiriusSignDocumentInfo info) {
    return new SiriusSignDocument._(
        info.fileName, info.fileType, null, null, info.fileHash, info);
  }

  SiriusSignDocument setOwner(PublicAccount publicAccount) {
    this.info.owner = SignerInfo(publicAccount.publicKey,
        publicAccount.address.address, false, null, null, null);
    return this;
  }

  SiriusSignDocument setCosigners(List<PublicAccount> cosigners) {
    this.info.cosigners = cosigners.map((cosigner) {
      SignerInfo info = SignerInfo(cosigner.publicKey, cosigner.address.address,
          false, null, null, null);
      return info;
    }).toList();
    return this;
  }

  SiriusSignDocument setVerifiers(List<PublicAccount> verifiers) {
    this.info.verifiers = verifiers.map((verifier) {
      SignerInfo info = SignerInfo(verifier.publicKey, verifier.address.address,
          false, null, null, null);
      return info;
    });
    return this;
  }

  void setFinalDocumentData(List<int> fileData) {
    this.completedDocumentData = fileData;
  }

  SiriusSignDocument setDocumentAccount(Account account) {
    this.documentAccount = account;
    this.info.documentAccount = account.publicAccount;
    this.info.id = account.publicKey;
    return this;
  }

  SiriusSignDocument setEncryptType(int encryptType) {
    this.info.encryptType = encryptType;
    return this;
  }

  SiriusSignDocument setUploadTxHash(String hash) {
    this.info.uploadTxHash = hash;
    return this;
  }

  SiriusSignDocument setSignatureUploadTxhash(
      PublicAccount signer, String hash) {
    this.signatureUploadTxHash = hash;
    this.info.addSignatureUploadTxHash(signer.address.address, hash);
    return this;
  }

  SiriusSignDocument setSignTxHash(
      PublicAccount signer, String hash, DateTime date) {
    this.info.addSignTxHash(signer.address.address, hash, date);
    return this;
  }

  toJson() {
    final Map<String, dynamic> json = {
      "documentAccount": this.documentAccount,
      "signatureUploadTxHash": this.signatureUploadTxHash,
      "completedDocumentData": this.completedDocumentData,
      "fileName": this.fileName,
      "fileType": this.fileType,
      "fileData": this.fileData,
      "fileDataUri": this.fileDataUri,
      "fileHash": this.fileHash,
      "info": {
        "id": this.info.id,
        "fileName": this.info.fileName,
        "fileType": this.info.fileType,
        "fileHash": this.info.fileHash,
        "uploadDate": this.info.uploadDate,
        "owner": this.info.owner.toJson(),
        "cosigners": this.info.cosigners.toString(),
        "documentAccount": this.info.documentAccount,
        "uploadTxHash": this.info.uploadTxHash,
        "encryptType": this.info.encryptType,
        "verifiers": this.info.verifiers,
        "isCompleted": this.info.isCompleted,
        "isVerified": this.info.isVerified
      },
    };
    return json;
  }
}
