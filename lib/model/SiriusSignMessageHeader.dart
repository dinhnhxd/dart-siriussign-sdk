part of siriussign_sdk;

class SiriusSignMessageHeader {
  final String appID;
  final String application;
  final String version;
  final int messageType;

  SiriusSignMessageHeader(
      this.appID, this.application, this.version, this.messageType);

  toJson() {
    final Map<String, dynamic> json = {
      "appID": this.appID,
      "application": this.application,
      "version": this.version,
      "messageType": this.messageType
    };
    return json;
  }
}
