part of siriussign_sdk;

/// Sirius Sign Document Signing Message that prove a document is signed by the transaction signer
class SiriusSignDocumentSigningMessage extends SiriusSignMessage {
  Map<String, dynamic> body = {
    "fileHash": String,
    "fileName": String,
    "fileType": String,
    "isOwner": bool,
    "uploadTxHash": String,
    "encryptType": int,
    "signatureUploadTxHash": String,
  };
  Map<String, dynamic> meta;

  SiriusSignDocumentSigningMessage(
      String appID,
      String fileHash,
      String fileName,
      String fileType,
      bool isOwner,
      String uploadTxHash,
      int encryptType,
      String signatureUploadTxHash,
      // signaturePositions: SignaturePosition[],
      [Map<String, dynamic> meta])
      : super(appID, SiriusSignMessageType.SIGN) {
    this.body = {
      "fileHash": fileHash,
      "fileName": fileName,
      "fileType": fileType,
      "isOwner": isOwner,
      "uploadTxHash": uploadTxHash,
      "encryptType": encryptType,
      "signatureUploadTxHash": signatureUploadTxHash,
      // signaturePositions: signaturePositions,
    };
    this.meta = meta != null ? meta : {};
  }

  get fileHash {
    return this.body["fileHash"];
  }

  get fileName {
    return this.body["fileName"];
  }

  get fileType {
    return this.body["fileType"];
  }

  get isOwner {
    return this.body["isOwner"];
  }

  get uploadTxHash {
    return this.body["uploadTxHash"];
  }

  get encryptType {
    return this.body["encryptType"];
  }

  get signatureUploadTxHash {
    return this.body["signatureUploadTxHash"];
  }

  static SiriusSignDocumentSigningMessage createFromPayload(String payload) {
    Map<String, dynamic> message;
    try {
      message = jsonDecode(payload);
    } catch (e) {
      throw ('M01: Payload is not a Sirius Sign message string');
    }
    if (message['header']['application'] != APPLICATION ?? true)
      throw ('M01: Payload is not a Sirius Sign message string');
    if (message['header']['messageType'] != SiriusSignMessageType.SIGN)
      throw ('M02: This Sirius Sign Message is not a Document Signing Message');

    return new SiriusSignDocumentSigningMessage(
        message['header']['appID'],
        message['body']['fileHash'],
        message['body']['fileName'],
        message['body']['fileType'],
        message['body']['isOwner'],
        message['body']['uploadTxHash'],
        message['body']['encryptType'],
        message['body']['signatureUploadTxHash'],
        message['meta']);
  }
}
