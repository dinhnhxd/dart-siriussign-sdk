part of siriussign_sdk.ipfs;

class Downloader {
  IpfsClient _ipfsClient;
  SiriusClient _siriusClient;

  Downloader(String chainNode, String ipfsNode) {
    this._ipfsClient = IpfsClient.fromUri(ipfsNode);
    this._siriusClient = SiriusClient.fromUrl(chainNode, null);
  }

  Future<DownloadResult> download(DownloadParameter param) async {
    final uploadTx = await this
        ._siriusClient
        .transaction
        .getTransaction(param.transactionHash);

    final tfTx = uploadTx as TransferTransaction;

    final uploadInfo =
        UploadMessage.fromJson(json.decode(tfTx.message.toJson()['payload']));

    final downloadResDat = DownloadResultData(
        this._ipfsClient,
        param.privacyStrategy,
        uploadInfo.dataHash,
        uploadInfo.timestamp,
        null,
        uploadInfo.description,
        uploadInfo.contentType,
        uploadInfo.name,
        uploadInfo.metadata);

    final downloadRes = DownloadResult(tfTx.transactionHash,
        uploadInfo.privacyType, uploadInfo.version, downloadResDat);
    return downloadRes;
  }
}
