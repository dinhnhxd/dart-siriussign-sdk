part of siriussign_sdk.ipfs;

const Key_Size = 32;
const Hash_Size = 64;

class CatapultHash {
  static final func = SHA3Hasher.func;
  static final createHasher = SHA3Hasher.createHasher;
}

class CatapultCrypto {
  static extractPublicKey(sk, hashfunc, int signSchema) {
    final d = prepareForScalarMult(sk, hashfunc, signSchema);

    final p = [Int64List(16), Int64List(16), Int64List(16), Int64List(16)];
    final pk = new Uint8List(Key_Size);
    CatapultNaclPublic.scalarbase(p, d);
    CatapultNaclPublic.pack(pk, p);
    return pk;
  }

  static Uint8List clamp(Uint8List d) {
    d[0] &= 248;
    d[31] &= 127;
    d[31] |= 64;
    return d;
  }

  static Uint8List prepareForScalarMult(sk, hashfunc, int signSchema) {
    Uint8List d = new Uint8List(Hash_Size);
    d = hashfunc(d, sk, Hash_Size, signSchema);
    d = clamp(d);
    return d;
  }

  static Uint8List deriveSharedKey(salt, sk, pk, hashfunc, int signSchema) {
    Uint8List d = prepareForScalarMult(sk, hashfunc, signSchema);

    // sharedKey = pack(p = d (derived from sk) * q (derived from pk))
    List<Int64List> q = [
      Int64List(16),
      Int64List(16),
      Int64List(16),
      Int64List(16)
    ];
    List<Int64List> p = [
      Int64List(16),
      Int64List(16),
      Int64List(16),
      Int64List(16)
    ];
    Uint8List sharedKey = new Uint8List(Key_Size);
    CatapultNaclPublic.unpackneg(q, pk);
    CatapultNaclPublic.scalarmult(p, q, d);
    CatapultNaclPublic.pack(sharedKey, p);
    // salt the shared key
    for (var i = 0; i < Key_Size; ++i) {
      sharedKey[i] ^= salt[i];
    }
    // return the hash of the result
    Uint8List sharedKeyHash = new Uint8List(Key_Size);
    sharedKeyHash = hashfunc(sharedKeyHash, sharedKey, Key_Size, signSchema);
    return sharedKeyHash;
  }
}
