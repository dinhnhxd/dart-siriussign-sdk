part of siriussign_sdk;

class FileInfo {
  String fileName;
  String fileType;
  Uint8List fileData;

  FileInfo(
    this.fileName,
    this.fileType,
    this.fileData,
  );
}

class DocumentSigning {
  String appID;
  Account userAccount;
  String apiNode;
  String ipfsNode;
  String networkGenerationHash;
  int networkType;
  dynamic websockketInject;
  int state = 0;
  bool isDone;
  bool isConfirmed;
  dynamic status;

  DocumentSigning(this.appID, this.userAccount, this.apiNode, this.ipfsNode,
      this.networkGenerationHash, this.networkType,
      [this.websockketInject]) {
    this._resetState();
  }

  _resetState() {
    this.state = 0;
    this.isDone = false;
    this.isConfirmed = false;
  }

  // Upload file
  //
  // If failed, set process done, status failed by error
  Future<SignedTransaction> uploadSignDoc(SiriusSignDocument document,
      [Uint8List completedDocumentData, String password]) async {
    DocumentStorage documentStorage =
        new DocumentStorage(this.apiNode, this.ipfsNode, this.networkType);

    final uploadRes = await documentStorage.uploadDocument(
        document, this.userAccount, password);

    if (completedDocumentData != null)
      documentStorage.uploadCompletedDoc(
          document.fileName,
          document.fileType,
          document.fileHash,
          completedDocumentData,
          this.userAccount,
          document.info.documentAccount,
          document.info.encryptType,
          password);

    return uploadRes;
  }

  /// Single sign with new process
  singleSign(SiriusSignDocument document,
      [FileInfo signatureImage,
      String password,
      Map<String, dynamic> meta]) async {
    final siriusSignTransaction = new SiriusSignTransaction(this.appID,
        this.userAccount, this.networkGenerationHash, this.networkType);
    final transactionListener =
        new TransactionListener(this.apiNode, this.websockketInject);
    final documentStorage =
        new DocumentStorage(this.apiNode, this.ipfsNode, this.networkType);
    this._resetState();
    if (document.info.cosigners.length > 0)
      throw ('S01: Cannot run singleSign with a multi-sign document');

    // Create document account
    document.setDocumentAccount(Account.random(this.networkType));

    // Set Restriction
    final signedRestrictionTx =
        siriusSignTransaction.createRestriction(document);
    final restrictTx = await transactionListener.waitForTransactionConfirmed(
        document.documentAccount.address, signedRestrictionTx.hash, () {
      transactionListener.announceTransaction(signedRestrictionTx);
    }).catchError((err) {
      this.isDone = true;
      this.status = err;
      return null;
    });
    if (restrictTx == null) return;
    this.state++;

    // Upload file
    if (document.completedDocumentData == null)
      document.setFinalDocumentData(document.fileData);
    final uploadTx = await this
        .uploadSignDoc(document, document.completedDocumentData, password);
    if (uploadTx == null) return;
    document.setUploadTxHash(uploadTx.hash);
    this.state++;

    // Upload signature
    if (signatureImage != null) {
      final signatureUploadTx = await documentStorage
          .uploadSignatureForDocSigning(
              signatureImage.fileName,
              signatureImage.fileType,
              signatureImage.fileData,
              this.userAccount,
              document.documentAccount.publicAccount)
          .catchError((err) {
        this.isDone = true;
        this.status = err.status;
        return null;
      });
      if (signatureUploadTx == null) return;
      document.setSignatureUploadTxhash(
          this.userAccount.publicAccount, signatureUploadTx.hash);
    } else {
      document.setSignatureUploadTxhash(this.userAccount.publicAccount, null);
    }
    this.state++;

    // Sign transaction
    // doc.setTxHash is performed inside create doc signing tx
    final signedDocSigningTx =
        siriusSignTransaction.createDocSigningTransaction(document, [], meta);
    final docSigningTx = await transactionListener.waitForTransactionConfirmed(
        this.userAccount.address, signedDocSigningTx.hash, () {
      transactionListener.announceTransaction(signedDocSigningTx);
    }).catchError((err) {
      this.isDone = true;
      this.status = err;
      return null;
    });
    if (docSigningTx == null) return;
    document.setSignTxHash(
        this.userAccount.publicAccount,
        (docSigningTx as TransferTransaction).transactionHash,
        docSigningTx.deadline.value
            .add(Duration(hours: DEADLINE_ADJUSTED_HOURS)));
    document.info.checkIsCompleted();
    this.state++;

    this.isDone = true;
    this.isConfirmed = true;
  }

  /// Run multi sign task
  multiSign(SiriusSignDocument document,
      [List<Mosaic> mosaics = const [],
      FileInfo signatureImage,
      Map<String, dynamic> signingMeta,
      List<Map<String, dynamic>> cosignerMetaArray,
      String password]) async {
    final siriusSignTransaction = new SiriusSignTransaction(this.appID,
        this.userAccount, this.networkGenerationHash, this.networkType);
    final transactionListener =
        new TransactionListener(this.apiNode, this.websockketInject);
    final documentStorage =
        new DocumentStorage(this.apiNode, this.ipfsNode, this.networkType);
    this._resetState();
    if (document.info.cosigners.length == 0)
      throw ('S02: Cannot run multiSign with a single-sign document');

    // Create document account
    document.setDocumentAccount(Account.random(this.networkType));

    // Set Restriction
    final signedRestrictionTx =
        siriusSignTransaction.createRestriction(document);
    final restrictTx = await transactionListener.waitForTransactionConfirmed(
        document.documentAccount.address, signedRestrictionTx.hash, () {
      transactionListener.announceTransaction(signedRestrictionTx);
    }).catchError((err) {
      this.isDone = true;
      this.status = err;
      return null;
    });
    if (restrictTx == null) return;
    this.state++;

    // Upload file
    final uploadTx = await this
        .uploadSignDoc(document, document.completedDocumentData, password);
    if (uploadTx == null) return;
    document.setUploadTxHash(uploadTx.hash);
    this.state++;

    // Upload signature
    if (signatureImage != null) {
      final signatureUploadTx = await documentStorage
          .uploadSignatureForDocSigning(
              signatureImage.fileName,
              signatureImage.fileType,
              signatureImage.fileData,
              this.userAccount,
              document.documentAccount.publicAccount)
          .catchError((err) {
        this.isDone = true;
        this.status = err.status;
        return null;
      });
      if (signatureUploadTx == null) return;
      document.setSignatureUploadTxhash(
          this.userAccount.publicAccount, signatureUploadTx.hash);
    } else {
      document.setSignatureUploadTxhash(this.userAccount.publicAccount, null);
    }
    this.state++;

    // Sign transaction
    final signedDocSigningTx = siriusSignTransaction
        .createDocSigningTransaction(document, mosaics, signingMeta);
    final docSigningTx = await transactionListener.waitForTransactionConfirmed(
        this.userAccount.address, signedDocSigningTx.hash, () {
      transactionListener.announceTransaction(signedDocSigningTx);
    }).catchError((err) {
      this.isDone = true;
      this.status = err;
      return null;
    });
    if (docSigningTx == null) return;
    SiriusClient client = SiriusClient.fromUrl(this.apiNode, null);
    TransferTransaction docSigningTfTx = await client.transaction
        .getTransaction((docSigningTx as TransferTransaction).transactionHash);
    document.setSignTxHash(
        this.userAccount.publicAccount,
        docSigningTfTx.transactionHash,
        docSigningTfTx.deadline.value
            .add(Duration(hours: DEADLINE_ADJUSTED_HOURS)));
    document.info.checkIsCompleted();
    this.state++;

    // Send notification
    final totalCosignersVerifiers =
        document.info.cosigners.length + document.info.verifiers.length;
    final notiMosaics = mosaics.map((Mosaic mosaic) {
      return new Mosaic(
          mosaic.assetId,
          Uint64.fromBigInt(BigInt.from(mosaic.amount.toBigInt() /
              BigInt.from(totalCosignersVerifiers))));
    }).toList();
    final signedNotiTx =
        siriusSignTransaction.createMultiSigningNotificationTransaction(
            document, notiMosaics, cosignerMetaArray);
    final Transaction aggNotiTx = await transactionListener
        .waitForTransactionConfirmed(
            document.documentAccount.address, signedNotiTx.hash, () {
      transactionListener.announceTransaction(signedNotiTx);
    }).catchError((err) {
      this.isDone = true;
      this.status = err;
      return null;
    });
    ;
    if (aggNotiTx == null) return;
    this.state++;

    this.isDone = true;
    this.isConfirmed = true;
  }

  /// Run Cosign task
  cosign(SiriusSignDocument document,
      [List<Mosaic> mosaics = const [],
      FileInfo signatureImage,
      Map<String, dynamic> meta,
      String password]) async {
    final siriusSignTransaction = new SiriusSignTransaction(this.appID,
        this.userAccount, this.networkGenerationHash, this.networkType);
    final transactionListener =
        new TransactionListener(this.apiNode, this.websockketInject);
    final documentStorage =
        new DocumentStorage(this.apiNode, this.ipfsNode, this.networkType);
    final documentFetch =
        new DocumentFetch(this.appID, this.apiNode, this.networkType);
    this._resetState();

    // Upload signature
    // int userSignatureIndex = this.selectedDocInfo.signerSignatures
    //     .map(signature => signature.publicKey)
    //     .indexOf(this.account.publicKey);
    // if (userSignatureIndex < 0) {
    //     userSignatureIndex = this.selectedDocInfo.signerSignatures
    //     .map(signature => signature.publicKey)
    //     .indexOf(this.account.address.plain());
    // }

    if (signatureImage != null) {
      this.state = 0;
      final signatureUploadTx = await documentStorage
          .uploadSignatureForDocSigning(
              signatureImage.fileName,
              signatureImage.fileType,
              signatureImage.fileData,
              this.userAccount,
              document.info.documentAccount)
          .catchError((err) {
        this.isDone = true;
        this.status = err.status;
        return null;
      });
      if (signatureUploadTx == null) return;
      // this.selectedDocInfo.signaturesUploadTxHash.push(signatureUploadTx.transactionHash);
      document.setSignatureUploadTxhash(
          this.userAccount.publicAccount, signatureUploadTx.hash);
    } else {
      document.setSignatureUploadTxhash(this.userAccount.publicAccount, null);
    }
    this.state++;

    // Sign transaction
    SignedTransaction signedDocSigningTx;
    signedDocSigningTx = siriusSignTransaction.createDocSigningTransaction(
        document, mosaics, meta);

    final TransferTransaction docSigningTx = await transactionListener
        .waitForTransactionConfirmed(
            this.userAccount.address, signedDocSigningTx.hash, () {
      transactionListener.announceTransaction(signedDocSigningTx);
    }).catchError((err) {
      this.isDone = true;
      this.status = err;
      return null;
    });
    if (docSigningTx == null) return;
    document.setSignTxHash(
        this.userAccount.publicAccount,
        docSigningTx.transactionHash,
        docSigningTx.deadline.value
            .add(Duration(hours: DEADLINE_ADJUSTED_HOURS)));
    document.info.checkIsCompleted();
    this.state++;

    // Upload completed document
    final allDocumentSigningTxs =
        await documentFetch.allDocumentSigningTransactionsByDocumentAccount(
            document.info.documentAccount.publicKey);
    // var sortCondition = (TransferTransaction a, TransferTransaction b) {
    //   if (a.id > b.id)
    //     return -1;
    //   else
    //     return 1;
    // };
    // allDocumentSigningTxs.sort((tx1, tx2) => sortCondition(tx1, tx2));

    // if (this.selectedDocInfo.cosignatures.length ==
    //     this.selectedDocInfo.cosigners.length - 1)
    //   this.uploadStorage.uploadCompletedDoc(
    //       this.completedFileUint8Array,
    //       this.name,
    //       this.selectedDocInfo.documentAccount,
    //       this.selectedDocInfo.isEncrypt,
    //       this.selectedDocInfo);

    if ((allDocumentSigningTxs[0].signer.publicKey ==
            this.userAccount.publicKey) &&
        (document.info.cosigners.length == allDocumentSigningTxs.length - 1)) {
      await documentStorage.uploadCompletedDoc(
          document.fileName,
          document.fileType,
          document.fileHash,
          document.completedDocumentData,
          this.userAccount,
          document.info.documentAccount,
          document.info.encryptType,
          password);
    }
    this.state++;

    this.isDone = true;
    this.isConfirmed = true;
  }

  /// Run verify task
  verify(SiriusSignDocument document,
      [List<Mosaic> mosaics = const [], Map<String, dynamic> meta]) async {
    final siriusSignTransaction = new SiriusSignTransaction(this.appID,
        this.userAccount, this.networkGenerationHash, this.networkType);
    final transactionListener =
        new TransactionListener(this.apiNode, this.websockketInject);
    this._resetState();

    final isCompleted = document.info.checkIsCompleted();
    if (!isCompleted) throw ('S03: Cannot verify an unsigned document');

    final signedVerifyTx = siriusSignTransaction.createDocSigningTransaction(
        document, mosaics, meta);
    final Transaction docVerifyTx = await transactionListener
        .waitForTransactionConfirmed(
            this.userAccount.address, signedVerifyTx.hash, () {
      transactionListener.announceTransaction(signedVerifyTx);
    }).catchError((err) {
      this.status = err.message;
      return null;
    });
    if (docVerifyTx == null) return;
    this.state++;

    this.isDone = true;
    this.isConfirmed = true;
  }
}
