part of siriussign_sdk;

class SiriusSignVerifierNotifyingMessage extends SiriusSignMessage {
  Map<String, dynamic> body = {
    "uploadTxHash": String,
  };

  Map<String, dynamic> meta;

  SiriusSignVerifierNotifyingMessage(String appID, String uploadTxHash,
      [Map<String, dynamic> meta])
      : super(appID, SiriusSignMessageType.VERIFY_NOTIFY) {
    this.body = {
      uploadTxHash: uploadTxHash,
    };
    this.meta = meta != null ? meta : {};
  }

  static SiriusSignVerifierNotifyingMessage createFromPayload(String payload) {
    Map<String, dynamic> message;
    try {
      message = jsonDecode(payload);
    } catch (e) {
      throw ('M01: Payload is not a Sirius Sign message string');
    }
    if (message['header']['application'] != APPLICATION)
      throw ('M01: Payload is not a Sirius Sign message string');
    if (message['header']['messageType'] != SiriusSignMessageType.VERIFY)
      throw ('M05: This Sirius Sign Message is not a Verifier Notifying Message');

    return new SiriusSignVerifierNotifyingMessage(message['header']['appID'],
        message['body']['uploadTxHash'], message['meta']);
  }
}
